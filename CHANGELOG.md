# StrawberryFields Laravel

- [3.0.0]
- [2.9.0] - 2024-08-10 - 11 files changed, 160 insertions(+), 94 deletions(-)
- [2.8.0] - 2024-03-10 - 5 files changed, 90 insertions(+), 25 deletions(-)
- [2.7.0] - 2024-02-04 - 9 files changed, 69 insertions(+), 23 deletions(-)
- [2.6.0] - 2023-11-11 - 7 files changed, 9281 insertions(+), 21 deletions(-)
- [2.5.0] - 2023-11-05 - 18 files changed, 657 insertions(+), 485 deletions(-)
- [2.4.0] - 2023-04-16 - 9 files changed, 507 insertions(+), 449 deletions(-)
- [2.3.0] - 2022-11-27 - 9 files changed, 7755 insertions(+), 13 deletions(-)
- [2.2.0] - 2022-11-20 - 6 files changed, 9 insertions(+), 7 deletions(-)
- [2.1.0] - 2022-06-26 - 5 files changed, 12 insertions(+), 22 deletions(-)
- [2.0.0] - 2022-03-13 - 15 files changed, 319 insertions(+), 89 deletions(-)
- [1.5.0] - 2021-10-11 - 6 files changed, 50 insertions(+), 5 deletions(-)
- [1.4.0] - 2021-10-01 - 4 files changed, 9 insertions(+), 2 deletions(-)
- [1.3.0] - 2021-09-25 - 4 files changed, 11 insertions(+), 8 deletions(-)
- [1.2.0] - 2021-04-26 - 4 files changed, 27 insertions(+), 19 deletions(-)
- [1.1.0] - 2021-03-23 - 3 files changed, 4 insertions(+), 5 deletions(-)
- 1.0.0 - 2021-03-04

[3.0.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/8e8c2821...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/d0ccd712...8e8c2821
[2.8.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/4074ccfb...d0ccd712
[2.7.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/c49d9122...4074ccfb
[2.6.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/4d6d0cb1...c49d9122
[2.5.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/e17b6b40...4d6d0cb1
[2.4.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/b531b396...e17b6b40
[2.3.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/1d021920...b531b396
[2.2.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/bb6962a0...1d021920
[2.1.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/b278693a...bb6962a0
[2.0.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/a7ea36fa...b278693a
[1.5.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/432bfd09...a7ea36fa
[1.4.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/6693f653...432bfd09
[1.3.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/346d4d54...6693f653
[1.2.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/98394db4...346d4d54
[1.1.0]: https://gitlab.com/acefed/strawberryfields-laravel/-/compare/f43c6a9c...98394db4
