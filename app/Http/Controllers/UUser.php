<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UUser extends Controller
{
    public function show($username, Request $request)
    {
        $hostname = parse_url($this::$CONFIG['origin'], PHP_URL_HOST);
        $accept_header_field = $request->header('Accept');
        $has_type = false;
        if ($username !== $this::$CONFIG['actor'][0]['preferredUsername']) return abort(404);
        if (strpos($accept_header_field, 'application/activity+json') !== false) $has_type = true;
        if (strpos($accept_header_field, 'application/ld+json') !== false) $has_type = true;
        if (strpos($accept_header_field, 'application/json') !== false) $has_type = true;
        if (!$has_type) {
            $body = "{$username}: {$this::$CONFIG['actor'][0]['name']}";
            $headers = [
                'Cache-Control' => "public, max-age={$this::$CONFIG['ttl']}, must-revalidate",
                'Vary' => 'Accept, Accept-Encoding',
                'Content-Type' => 'text/plain',
            ];
            return response($body)->withHeaders($headers);
        }
        $body = [
            '@context' => [
                'https://www.w3.org/ns/activitystreams',
                'https://w3id.org/security/v1',
                [
                    'schema' => 'https://schema.org/',
                    'PropertyValue' => 'schema:PropertyValue',
                    'value' => 'schema:value',
                    'Key' => 'sec:Key',
                ],
            ],
            'id' => "https://{$hostname}/u/{$username}",
            'type' => 'Person',
            'inbox' => "https://{$hostname}/u/{$username}/inbox",
            'outbox' => "https://{$hostname}/u/{$username}/outbox",
            'following' => "https://{$hostname}/u/{$username}/following",
            'followers' => "https://{$hostname}/u/{$username}/followers",
            'preferredUsername' => $username,
            'name' => $this::$CONFIG['actor'][0]['name'],
            'summary' => '<p>3.0.0</p>',
            'url' => "https://{$hostname}/u/{$username}",
            'endpoints' => ['sharedInbox' => "https://{$hostname}/u/{$username}/inbox"],
            'attachment' => [
                [
                  'type' => 'PropertyValue',
                  'name' => 'me',
                  'value' => $this::$ME,
                ],
            ],
            'icon' => [
                'type' => 'Image',
                'mediaType' => 'image/png',
                'url' => "https://{$hostname}/public/{$username}u.png",
            ],
            'image' => [
                'type' => 'Image',
                'mediaType' => 'image/png',
                'url' => "https://{$hostname}/public/{$username}s.png",
            ],
            'publicKey' => [
                'id' => "https://{$hostname}/u/{$username}#Key",
                'type' => 'Key',
                'owner' => "https://{$hostname}/u/{$username}",
                'publicKeyPem' => $this::$public_key_pem,
            ],
        ];
        $headers = [
            'Cache-Control' => "public, max-age={$this::$CONFIG['ttl']}, must-revalidate",
            'Vary' => 'Accept, Accept-Encoding',
            'Content-Type' => 'application/activity+json',
        ];
        return response()->json($body, 200, $headers, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}
