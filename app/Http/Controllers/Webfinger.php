<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Webfinger extends Controller
{
    public function show(Request $request)
    {
        $username = $this::$CONFIG['actor'][0]['preferredUsername'];
        $hostname = parse_url($this::$CONFIG['origin'], PHP_URL_HOST);
        $p443 = "https://{$hostname}:443/";
        $resource = $request->query('resource');
        $has_resource = false;
        if (substr($resource, 0, strlen($p443)) === $p443) {
            $resource = "https://{$hostname}/" . substr($resource, strlen($p443));
        }
        if ($resource === "acct:{$username}@{$hostname}") $has_resource = true;
        if ($resource === "mailto:{$username}@{$hostname}") $has_resource = true;
        if ($resource === "https://{$hostname}/@{$username}") $has_resource = true;
        if ($resource === "https://{$hostname}/u/{$username}") $has_resource = true;
        if ($resource === "https://{$hostname}/user/{$username}") $has_resource = true;
        if ($resource === "https://{$hostname}/users/{$username}") $has_resource = true;
        if (!$has_resource) return abort(404);
        $body = [
            'subject' => "acct:{$username}@{$hostname}",
            'aliases' => [
                "mailto:{$username}@{$hostname}",
                "https://{$hostname}/@{$username}",
                "https://{$hostname}/u/{$username}",
                "https://{$hostname}/user/{$username}",
                "https://{$hostname}/users/{$username}",
            ],
            'links' => [
                [
                    'rel' => 'self',
                    'type' => 'application/activity+json',
                    'href' => "https://{$hostname}/u/{$username}",
                ],
                [
                    'rel' => 'http://webfinger.net/rel/avatar',
                    'type' => 'image/png',
                    'href' => "https://{$hostname}/public/{$username}u.png",
                ],
                [
                    'rel' => 'http://webfinger.net/rel/profile-page',
                    'type' => 'text/plain',
                    'href' => "https://{$hostname}/u/{$username}",
                ],
            ],
        ];
        $headers = [
            'Cache-Control' => "public, max-age={$this::$CONFIG['ttl']}, must-revalidate",
            'Vary' => 'Accept, Accept-Encoding',
            'Content-Type' => 'application/jrd+json',
        ];
        return response()->json($body, 200, $headers, JSON_UNESCAPED_SLASHES);
    }
}
