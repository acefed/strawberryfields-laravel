<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SSend extends Controller
{
    public function store($secret, $username, Request $request)
    {
        $hostname = parse_url($this::$CONFIG['origin'], PHP_URL_HOST);
        $send = $request->input();
        $t = $send['type'] ?? '';
        if ($username !== $this::$CONFIG['actor'][0]['preferredUsername']) return abort(404);
        if (!$secret || $secret === '-') return abort(404);
        if ($secret !== getenv('SECRET')) return abort(404);
        if (parse_url($send['id'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
        $x = $this::getActivity($username, $hostname, $send['id']);
        if (!$x) return abort(500);
        $aid = $x['id'] ?? '';
        $atype = $x['type'] ?? '';
        if (strlen($aid) > 1024 || strlen($atype) > 64) return abort(400);
        if ($t === 'follow') {
            $this::follow($username, $hostname, $x);
            return response('', 200);
        }
        if ($t === 'undo_follow') {
            $this::undoFollow($username, $hostname, $x);
            return response('', 200);
        }
        if ($t === 'like') {
            if (parse_url($x['attributedTo'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
            $y = $this::getActivity($username, $hostname, $x['attributedTo']);
            if (!$y) return abort(500);
            $this::like($username, $hostname, $x, $y);
            return response('', 200);
        }
        if ($t === 'undo_like') {
            if (parse_url($x['attributedTo'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
            $y = $this::getActivity($username, $hostname, $x['attributedTo']);
            if (!$y) return abort(500);
            $this::undoLike($username, $hostname, $x, $y);
            return response('', 200);
        }
        if ($t === 'announce') {
            if (parse_url($x['attributedTo'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
            $y = $this::getActivity($username, $hostname, $x['attributedTo']);
            if (!$y) return abort(500);
            $this::announce($username, $hostname, $x, $y);
            return response('', 200);
        }
        if ($t === 'undo_announce') {
            if (parse_url($x['attributedTo'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
            $y = $this::getActivity($username, $hostname, $x['attributedTo']);
            if (!$y) return abort(500);
            if (!empty($send['url'])) {
                $z = $send['url'];
            } else {
                $z = "https://{$hostname}/u/{$username}/s/00000000000000000000000000000000";
            }
            if (parse_url($z, PHP_URL_SCHEME) !== 'https') return abort(400);
            $this::undoAnnounce($username, $hostname, $x, $y, $z);
            return response('', 200);
        }
        if ($t === 'create_note') {
            if (!empty($send['url'])) {
                $y = $send['url'];
            } else {
                $y = 'https://localhost';
            }
            if (parse_url($y, PHP_URL_SCHEME) !== 'https') return abort(400);
            $this::createNote($username, $hostname, $x, $y);
            return response('', 200);
        }
        if ($t === 'create_note_image') {
            if (!empty($send['url'])) {
                $y = $send['url'];
            } else {
                $y = "https://{$hostname}/public/logo.png";
            }
            if (parse_url($y, PHP_URL_SCHEME) !== 'https') return abort(400);
            if (parse_url($y, PHP_URL_HOST) !== $hostname) return abort(400);
            $z = 'image/png';
            if (substr($y, -4) === '.jpg' || substr($y, -5) === '.jpeg') $z = 'image/jpeg';
            if (substr($y, -4) === '.svg') $z = 'image/svg+xml';
            if (substr($y, -4) === '.gif') $z = 'image/gif';
            if (substr($y, -5) === '.webp') $z = 'image/webp';
            if (substr($y, -5) === '.avif') $z = 'image/avif';
            $this::createNoteImage($username, $hostname, $x, $y, $z);
            return response('', 200);
        }
        if ($t === 'create_note_mention') {
            if (parse_url($x['attributedTo'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
            $y = $this::getActivity($username, $hostname, $x['attributedTo']);
            if (!$y) return abort(500);
            if (!empty($send['url'])) {
                $z = $send['url'];
            } else {
                $z = 'https://localhost';
            }
            if (parse_url($z, PHP_URL_SCHEME) !== 'https') return abort(400);
            $this::createNoteMention($username, $hostname, $x, $y, $z);
            return response('', 200);
        }
        if ($t === 'create_note_hashtag') {
            if (!empty($send['url'])) {
                $y = $send['url'];
            } else {
                $y = 'https://localhost';
            }
            if (parse_url($y, PHP_URL_SCHEME) !== 'https') return abort(400);
            if (!empty($send['tag'])) {
                $z = $send['tag'];
            } else {
                $z = 'Hashtag';
            }
            $this::createNoteHashtag($username, $hostname, $x, $y, $z);
            return response('', 200);
        }
        if ($t === 'update_note') {
            if (!empty($send['url'])) {
                $y = $send['url'];
            } else {
                $y = "https://{$hostname}/u/{$username}/s/00000000000000000000000000000000";
            }
            if (parse_url($y, PHP_URL_SCHEME) !== 'https') return abort(400);
            $this::updateNote($username, $hostname, $x, $y);
            return response('', 200);
        }
        if ($t === 'delete_tombstone') {
            if (!empty($send['url'])) {
                $y = $send['url'];
            } else {
                $y = "https://{$hostname}/u/{$username}/s/00000000000000000000000000000000";
            }
            if (parse_url($y, PHP_URL_SCHEME) !== 'https') return abort(400);
            $this::deleteTombstone($username, $hostname, $x, $y);
            return response('', 200);
        }
        error_log("TYPE {$aid} {$atype}");
        return response('', 200);
    }
}
