<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Followers extends Controller
{
    public function show($username, Request $request)
    {
        $hostname = parse_url($this::$CONFIG['origin'], PHP_URL_HOST);
        if ($username !== $this::$CONFIG['actor'][0]['preferredUsername']) return abort(404);
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/followers",
            'type' => 'OrderedCollection',
            'totalItems' => 0,
        ];
        $headers = ['Content-Type' => 'application/activity+json'];
        return response()->json($body, 200, $headers, JSON_UNESCAPED_SLASHES);
    }
}
