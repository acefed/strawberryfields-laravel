<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

// Controllers
// |-- About.php
// |-- Controller.php
// |-- Followers.php
// |-- Following.php
// |-- Home.php
// |-- Inbox.php
// |-- Nodeinfo.php
// |-- Outbox.php
// |-- SSend.php
// |-- UUser.php
// `-- Webfinger.php

abstract class Controller
{
    public static $CONFIG;
    public static $ME;
    public static $PRIVATE_KEY;
    public static $public_key_pem;

    public function __construct()
    {
        if (getenv('CONFIG_JSON')) {
            $config_json = getenv('CONFIG_JSON');
            if (substr($config_json, 0, 1) === "'") $config_json = substr($config_json, 1);
            if (substr($config_json, -1) === "'") $config_json = substr($config_json, 0, -1);
        } else {
            if (file_exists('data/config.json')) $config_json = file_get_contents('data/config.json');
            else $config_json = file_get_contents('../data/config.json');
        }
        self::$CONFIG = json_decode($config_json, true);
        self::$ME = implode([
            '<a href="https://',
            parse_url(self::$CONFIG['actor'][0]['me'], PHP_URL_HOST),
            '/" rel="me nofollow noopener noreferrer" target="_blank">',
            'https://',
            parse_url(self::$CONFIG['actor'][0]['me'], PHP_URL_HOST),
            '/',
            '</a>',
        ]);
        $private_key_pem = getenv('PRIVATE_KEY');
        $private_key_pem = implode("\n", explode("\\n", $private_key_pem));
        if (substr($private_key_pem, 0, 1) === '"') $private_key_pem = substr($private_key_pem, 1);
        if (substr($private_key_pem, -1) === '"') $private_key_pem = substr($private_key_pem, 0, -1);
        self::$PRIVATE_KEY = openssl_pkey_get_private($private_key_pem);
        self::$public_key_pem = openssl_pkey_get_details(self::$PRIVATE_KEY)['key'];
    }

    public static function uuidv7()
    {
        $v = random_bytes(16);
        $ts = intval(microtime(true) * 1000);
        $v[0] = chr($ts >> 40);
        $v[1] = chr($ts >> 32);
        $v[2] = chr($ts >> 24);
        $v[3] = chr($ts >> 16);
        $v[4] = chr($ts >> 8);
        $v[5] = chr($ts);
        $v[6] = chr(ord($v[6]) & 0x0f | 0x70);
        $v[8] = chr(ord($v[8]) & 0x3f | 0x80);
        return bin2hex($v);
    }

    public static function talkScript($req)
    {
        $ts = intval(microtime(true) * 1000);
        if (parse_url($req, PHP_URL_HOST) === 'localhost') return "<p>{$ts}</p>";
        return implode([
            '<p>',
            '<a href="https://',
            parse_url($req, PHP_URL_HOST),
            '/" rel="nofollow noopener noreferrer" target="_blank">',
            parse_url($req, PHP_URL_HOST),
            '</a>',
            '</p>',
        ]);
    }

    public static function getActivity($username, $hostname, $req)
    {
        $t = date('D, d M Y H:i:s') . ' GMT';
        openssl_sign(
            implode("\n", [
                '(request-target): get ' . parse_url($req, PHP_URL_PATH),
                'host: ' . parse_url($req, PHP_URL_HOST),
                "date: {$t}",
            ]),
            $sig,
            self::$PRIVATE_KEY,
            OPENSSL_ALGO_SHA256,
        );
        $b64 = base64_encode($sig);
        $headers = [
            'Date' => $t,
            'Signature' => implode(',', [
                "keyId=\"https://{$hostname}/u/{$username}#Key\"",
                'algorithm="rsa-sha256"',
                'headers="(request-target) host date"',
                "signature=\"{$b64}\"",
            ]),
            'Accept' => 'application/activity+json',
            'Accept-Encoding' => 'identity',
            'Cache-Control' => 'no-cache',
            'User-Agent' => "StrawberryFields-Laravel/3.0.0 (+https://{$hostname}/)",
        ];
        $res = Http::withHeaders($headers)->get($req);
        $status = $res->getStatusCode();
        error_log("GET {$req} {$status}");
        return $res->json();
    }

    public static function postActivity($username, $hostname, $req, $x)
    {
        $t = date('D, d M Y H:i:s') . ' GMT';
        $body = json_encode($x, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $s256 = base64_encode(hash('sha256', $body, true));
        openssl_sign(
            implode("\n", [
                '(request-target): post ' . parse_url($req, PHP_URL_PATH),
                'host: ' . parse_url($req, PHP_URL_HOST),
                "date: {$t}",
                "digest: SHA-256={$s256}",
            ]),
            $sig,
            self::$PRIVATE_KEY,
            OPENSSL_ALGO_SHA256,
        );
        $b64 = base64_encode($sig);
        $headers = [
            'Date' => $t,
            'Digest' => "SHA-256={$s256}",
            'Signature' => implode(',', [
                "keyId=\"https://{$hostname}/u/{$username}#Key\"",
                'algorithm="rsa-sha256"',
                'headers="(request-target) host date digest"',
                "signature=\"{$b64}\"",
            ]),
            'Accept' => 'application/json',
            'Accept-Encoding' => 'gzip',
            'Cache-Control' => 'max-age=0',
            'User-Agent' => "StrawberryFields-Laravel/3.0.0 (+https://{$hostname}/)",
        ];
        error_log("POST {$req} {$body}");
        Http::withHeaders($headers)->withBody($body, 'application/activity+json')->post($req);
    }

    public static function acceptFollow($username, $hostname, $x, $y)
    {
        $aid = self::uuidv7();
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
            'type' => 'Accept',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => $y,
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function follow($username, $hostname, $x)
    {
        $aid = self::uuidv7();
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
            'type' => 'Follow',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => $x['id'],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function undoFollow($username, $hostname, $x)
    {
        $aid = self::uuidv7();
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}#Undo",
            'type' => 'Undo',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => [
                'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'type' => 'Follow',
                'actor' => "https://{$hostname}/u/{$username}",
                'object' => $x['id'],
            ],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function like($username, $hostname, $x, $y)
    {
        $aid = self::uuidv7();
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
            'type' => 'Like',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => $x['id'],
        ];
        self::postActivity($username, $hostname, $y['inbox'], $body);
    }

    public static function undoLike($username, $hostname, $x, $y)
    {
        $aid = self::uuidv7();
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}#Undo",
            'type' => 'Undo',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => [
                'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'type' => 'Like',
                'actor' => "https://{$hostname}/u/{$username}",
                'object' => $x['id'],
            ],
        ];
        self::postActivity($username, $hostname, $y['inbox'], $body);
    }

    public static function announce($username, $hostname, $x, $y)
    {
        $aid = self::uuidv7();
        $t = date('Y-m-d\TH:i:s\Z');
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}/activity",
            'type' => 'Announce',
            'actor' => "https://{$hostname}/u/{$username}",
            'published' => $t,
            'to' => ['https://www.w3.org/ns/activitystreams#Public'],
            'cc' => ["https://{$hostname}/u/{$username}/followers"],
            'object' => $x['id'],
        ];
        self::postActivity($username, $hostname, $y['inbox'], $body);
    }

    public static function undoAnnounce($username, $hostname, $x, $y, $z)
    {
        $aid = self::uuidv7();
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}#Undo",
            'type' => 'Undo',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => [
                'id' => "{$z}/activity",
                'type' => 'Announce',
                'actor' => "https://{$hostname}/u/{$username}",
                'to' => ['https://www.w3.org/ns/activitystreams#Public'],
                'cc' => ["https://{$hostname}/u/{$username}/followers"],
                'object' => $x['id'],
            ],
        ];
        self::postActivity($username, $hostname, $y['inbox'], $body);
    }

    public static function createNote($username, $hostname, $x, $y)
    {
        $aid = self::uuidv7();
        $t = date('Y-m-d\TH:i:s\Z');
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}/activity",
            'type' => 'Create',
            'actor' => "https://{$hostname}/u/{$username}",
            'published' => $t,
            'to' => ['https://www.w3.org/ns/activitystreams#Public'],
            'cc' => ["https://{$hostname}/u/{$username}/followers"],
            'object' => [
                'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'type' => 'Note',
                'attributedTo' => "https://{$hostname}/u/{$username}",
                'content' => self::talkScript($y),
                'url' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'published' => $t,
                'to' => ['https://www.w3.org/ns/activitystreams#Public'],
                'cc' => ["https://{$hostname}/u/{$username}/followers"],
            ],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function createNoteImage($username, $hostname, $x, $y, $z)
    {
        $aid = self::uuidv7();
        $t = date('Y-m-d\TH:i:s\Z');
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}/activity",
            'type' => 'Create',
            'actor' => "https://{$hostname}/u/{$username}",
            'published' => $t,
            'to' => ['https://www.w3.org/ns/activitystreams#Public'],
            'cc' => ["https://{$hostname}/u/{$username}/followers"],
            'object' => [
                'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'type' => 'Note',
                'attributedTo' => "https://{$hostname}/u/{$username}",
                'content' => self::talkScript('https://localhost'),
                'url' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'published' => $t,
                'to' => ['https://www.w3.org/ns/activitystreams#Public'],
                'cc' => ["https://{$hostname}/u/{$username}/followers"],
                'attachment' => [
                    [
                        'type' => 'Image',
                        'mediaType' => $z,
                        'url' => $y,
                    ],
                ],
            ],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function createNoteMention($username, $hostname, $x, $y, $z)
    {
        $aid = self::uuidv7();
        $t = date('Y-m-d\TH:i:s\Z');
        $at = "@{$y['preferredUsername']}@" . parse_url($y['inbox'], PHP_URL_HOST);
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}/activity",
            'type' => 'Create',
            'actor' => "https://{$hostname}/u/{$username}",
            'published' => $t,
            'to' => ['https://www.w3.org/ns/activitystreams#Public'],
            'cc' => ["https://{$hostname}/u/{$username}/followers"],
            'object' => [
                'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'type' => 'Note',
                'attributedTo' => "https://{$hostname}/u/{$username}",
                'inReplyTo' => $x['id'],
                'content' => self::talkScript($z),
                'url' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'published' => $t,
                'to' => ['https://www.w3.org/ns/activitystreams#Public'],
                'cc' => ["https://{$hostname}/u/{$username}/followers"],
                'tag' => [
                    [
                        'type' => 'Mention',
                        'name' => $at,
                    ],
                ],
            ],
        ];
        self::postActivity($username, $hostname, $y['inbox'], $body);
    }

    public static function createNoteHashtag($username, $hostname, $x, $y, $z)
    {
        $aid = self::uuidv7();
        $t = date('Y-m-d\TH:i:s\Z');
        $body = [
            '@context' => ['https://www.w3.org/ns/activitystreams', ['Hashtag' => 'as:Hashtag']],
            'id' => "https://{$hostname}/u/{$username}/s/{$aid}/activity",
            'type' => 'Create',
            'actor' => "https://{$hostname}/u/{$username}",
            'published' => $t,
            'to' => ['https://www.w3.org/ns/activitystreams#Public'],
            'cc' => ["https://{$hostname}/u/{$username}/followers"],
            'object' => [
                'id' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'type' => 'Note',
                'attributedTo' => "https://{$hostname}/u/{$username}",
                'content' => self::talkScript($y),
                'url' => "https://{$hostname}/u/{$username}/s/{$aid}",
                'published' => $t,
                'to' => ['https://www.w3.org/ns/activitystreams#Public'],
                'cc' => ["https://{$hostname}/u/{$username}/followers"],
                'tag' => [
                    [
                        'type' => 'Hashtag',
                        'name' => "#{$z}",
                    ],
                ],
            ],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function updateNote($username, $hostname, $x, $y)
    {
        $t = date('Y-m-d\TH:i:s\Z');
        $ts = intval(microtime(true) * 1000);
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "{$y}#{$ts}",
            'type' => 'Update',
            'actor' => "https://{$hostname}/u/{$username}",
            'published' => $t,
            'to' => ['https://www.w3.org/ns/activitystreams#Public'],
            'cc' => ["https://{$hostname}/u/{$username}/followers"],
            'object' => [
                'id' => $y,
                'type' => 'Note',
                'attributedTo' => "https://{$hostname}/u/{$username}",
                'content' => self::talkScript('https://localhost'),
                'url' => $y,
                'updated' => $t,
                'to' => ['https://www.w3.org/ns/activitystreams#Public'],
                'cc' => ["https://{$hostname}/u/{$username}/followers"],
            ],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }

    public static function deleteTombstone($username, $hostname, $x, $y)
    {
        $body = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "{$y}#Delete",
            'type' => 'Delete',
            'actor' => "https://{$hostname}/u/{$username}",
            'object' => [
                'id' => $y,
                'type' => 'Tombstone',
            ],
        ];
        self::postActivity($username, $hostname, $x['inbox'], $body);
    }
}
