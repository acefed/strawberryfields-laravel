<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\About;
use App\Http\Controllers\UUser;
use App\Http\Controllers\Inbox;
use App\Http\Controllers\Outbox;
use App\Http\Controllers\Following;
use App\Http\Controllers\Followers;
use App\Http\Controllers\SSend;
use App\Http\Controllers\Nodeinfo;
use App\Http\Controllers\Webfinger;

Route::get('/', [Home::class, 'index']);

Route::get('/about', [About::class, 'show']);

Route::get('u/{username}', [UUser::class, 'show']);

Route::get('u/{username}/inbox', [Inbox::class, 'show']);
Route::post('u/{username}/inbox', [Inbox::class, 'store']);

Route::post('u/{username}/outbox', [Outbox::class, 'store']);
Route::get('u/{username}/outbox', [Outbox::class, 'show']);

Route::get('u/{username}/following', [Following::class, 'show']);

Route::get('u/{username}/followers', [Followers::class, 'show']);

Route::post('s/{secret}/u/{username}', [SSend::class, 'store']);

Route::get('.well-known/nodeinfo', [Nodeinfo::class, 'show']);

Route::get('.well-known/webfinger', [Webfinger::class, 'show']);

Route::get('@', function () {
    return redirect('/');
});
Route::get('u', function () {
    return redirect('/');
});
Route::get('user', function () {
    return redirect('/');
});
Route::get('users', function () {
    return redirect('/');
});

Route::get('users/{username}', function ($username) {
    return redirect("u/{$username}");
});
Route::get('user/{username}', function ($username) {
    return redirect("u/{$username}");
});
Route::get('@{username}', function ($username) {
    return redirect("u/{$username}");
});
